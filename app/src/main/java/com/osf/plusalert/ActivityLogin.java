package com.osf.plusalert;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.osf.plusalert.public_resources.CommandCenter;


public class ActivityLogin extends AppCompatActivity {

    CommandCenter commandCenter = CommandCenter.getInstance();

    private EditText txtBoxName;
    private EditText txtBoxPhone;
    private EditText txtBoxEmail;
    private Button button;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        buildContent();
    }

    private void buildContent() {
        txtBoxName =  findViewById(R.id.editText);
        txtBoxPhone =  findViewById(R.id.editText2);
        txtBoxEmail =  findViewById(R.id.editText3);
        button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkValidity();
            }
        });
    }

    private void checkValidity() {
        if (!txtBoxName.getText().toString().equals("")
                && !txtBoxPhone.getText().toString().equals("")
                && !txtBoxEmail.getText().toString().equals("")) {
            commandCenter.sendUser(txtBoxName.getText().toString(), txtBoxPhone.getText().toString(),
                    txtBoxEmail.getText().toString());

            Intent intent = new Intent(ActivityLogin.this, MapActivity.class);
            startActivity(intent);
        } else {
            //TODO: show error message
        }
    }
}