package com.osf.plusalert.public_resources;

import android.os.AsyncTask;
import android.util.Log;

import com.osf.plusalert.enumerations.EAlertType;
import com.osf.plusalert.enumerations.EDisasterType;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class CommandCenter {

    private static CommandCenter INSTANCE;
    private static final String GET_ALERTS_REQUEST = "https://webmanagementapphackathon.azurewebsites.net/Api/GetAlerts?l1=1&l2=2";
    private static final String GET_REFUGES_REQUEST = "https://webmanagementapphackathon.azurewebsites.net/Api/GetRefuges?l1=1&l2=1";
    private static final String SEND_MESSAGE = "https://webmanagementapphackathon.azurewebsites.net/Api/CreateMessage?";
    private static final String SEND_USER = "https://webmanagementapphackathon.azurewebsites.net/Api/CreateClient?";
    private static final String USER_EXISTS = "https://webmanagementapphackathon.azurewebsites.net/Api/CheckClient?";
    private static final String SET_CLIENT_OFFLINE = "https://webmanagementapphackathon.azurewebsites.net/Api/SetClientOffline?";
    private static final String SET_CLIENT_ONLINE = "https://webmanagementapphackathon.azurewebsites.net/Api/SetClientOnline?";
    private static final String UPDATE_CLIENT_LOCATION = "https://webmanagementapphackathon.azurewebsites.net/Api/UpdateClientLocation?";

    public String imei = null;
    public Double actualLatitude;
    public Double actualLongitude;

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    private CommandCenter() {

    }

    public static CommandCenter getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CommandCenter();
        }

        return INSTANCE;
    }

    public void updateClientLocation() {
        try {
            StringBuilder userExists = new StringBuilder(UPDATE_CLIENT_LOCATION);

            userExists.append("imei=" + imei + "&");
            userExists.append("latitude=" + actualLatitude + "&");
            userExists.append("longitude=" + actualLongitude);

            String url = userExists.toString();
            new JsonTask().execute(url).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void setUserOffline(String imei) {
        try {
            StringBuilder userExists = new StringBuilder(SET_CLIENT_OFFLINE);

            userExists.append("imei=" + imei);
            String url = userExists.toString();

            new JsonTask().execute(url).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void setUserOnline(String imei) {
        try {
            StringBuilder userExists = new StringBuilder(SET_CLIENT_ONLINE);

            userExists.append("imei=" + imei);
            String url = userExists.toString();

            new JsonTask().execute(url).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }


    public boolean checkIfUserExists(String imei) {
        String result = null;

        try {
            StringBuilder userExists = new StringBuilder(USER_EXISTS);

            userExists.append("imei=" + imei);
            String url = userExists.toString();

            result = new JsonTask().execute(url).get();
            result = result.trim();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return result.equals("true") ? true : false;
    }

    public void sendUser(String name, String phone, String email) {
        try {
            StringBuilder message = new StringBuilder(SEND_USER);

            message.append("imei=" + imei + "&");
            message.append("name=" + name + "&");
            message.append("phoneNumber=" + phone + "&");
            message.append("email=" + email + "&");
            message.append("latitude=" + actualLatitude + "&");
            message.append("longitude=" + actualLongitude);

            String url = message.toString();
            // Send user
            new JsonTask().execute(url).get();
            int x = 1;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void sendAlert(IEntity entity, String imei) {
        try {
            if (entity instanceof Alert) {

                Alert alert = (Alert) entity;
                StringBuilder message = new StringBuilder(SEND_MESSAGE);

                message.append("imei=" + imei + "&");
                message.append("name=" + alert.getEntityName() + "&");
                message.append("description=" + alert.getDescription() + "&");
                message.append("latitude=" + alert.getLatitude() + "&");
                message.append("longitude=" + alert.getLongitude() + "&");
                message.append("radius=" + 10 + "&");
                message.append("disasterType=" + alert.getDisasterType().getAsString());

                String url = message.toString();
                // Send message
                new JsonTask().execute(url).get();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public List<IRefuge> getRefuges() throws JSONException {
        List<IRefuge> refuges = new ArrayList<>();

        try {
            String jsons = new JsonTask().execute(GET_REFUGES_REQUEST).get();
            jsons = jsons.replace("[", "");
            jsons = jsons.replace("]", "");
            jsons = jsons.replace("\n", "");

            String[] tokens = jsons.split("(?<=\\}),(?=\\{)");
            for (String token : tokens) {
                IRefuge refuge = getRefugeFromJson(new JSONObject(token));
                refuges.add(refuge);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return refuges;
    }

    public List<IAlert> getAlerts() throws JSONException {
        List<IAlert> alerts = new ArrayList<>();

        try {
            String jsons = new JsonTask().execute(GET_ALERTS_REQUEST).get();
            jsons = jsons.replace("[", "");
            jsons = jsons.replace("]", "");
            jsons = jsons.replace("\n", "");

            String[] tokens = jsons.split("(?<=\\}),(?=\\{)");
            for (String token : tokens) {
                IAlert alert = getAlertFromJson(new JSONObject(token));
                alerts.add(alert);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return alerts;
    }

    private IRefuge getRefugeFromJson(JSONObject json) throws JSONException {
        Double longitude = Double.valueOf(json.get("longitude").toString());
        Double latitude = Double.valueOf(json.get("latitude").toString());
        String refugeName = json.get("name").toString();
        Integer capacity = Integer.valueOf(json.get("capacity").toString());

        return new Refuge(latitude, longitude, refugeName, capacity);
    }

    private IAlert getAlertFromJson(JSONObject json) throws JSONException {
        Double longitude = Double.valueOf(json.get("longitude").toString());
        Double latitude = Double.valueOf(json.get("latitude").toString());
        String alertName = json.get("name").toString();
        String alertType = json.get("alertType").toString();
        String disasterType = json.get("disasterType").toString();
        String description = json.get("description").toString();
        Double radius = Double.valueOf(json.get("radius").toString());

        return new Alert(latitude, longitude, EAlertType.getValue(alertType), alertName, description,
                radius, EDisasterType.getValue(disasterType));
    }

    public void setActualLongitude(Double actualLongitude) {
        this.actualLongitude = actualLongitude;
    }

    public void setActualLatitude(Double actualLatitude) {
        this.actualLatitude = actualLatitude;
    }

    public Double getActualLongitude() {
        return actualLongitude;
    }

    public Double getActualLatitude() {
        return actualLatitude;
    }

    private class JsonTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(strings[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line); //Here u ll get whole response
                }

                return buffer.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            String text = result;
        }
    }
}
