package com.osf.plusalert.public_resources;

public class Refuge implements IRefuge{

    private final Double latitude;
    private final Double longitude;
    private final String name;
    private final Integer capacity;

    public Refuge(double latitude, double longitude, String name, int capacity){
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.capacity = capacity;
    }

    public Double getLatitude(){
        return latitude;
    }

    public Double getLongitude(){
        return longitude;
    }

    @Override
    public String getEntityName() {
        return name;
    }

    @Override
    public Integer getCapacity() {
        return capacity;
    }
}
