package com.osf.plusalert.public_resources;

public interface IRefuge extends IEntity {
    Integer getCapacity();
}
