package com.osf.plusalert.public_resources;

import com.osf.plusalert.enumerations.EAlertType;

public interface IAlert extends IEntity {
    EAlertType getAlertType();
}
