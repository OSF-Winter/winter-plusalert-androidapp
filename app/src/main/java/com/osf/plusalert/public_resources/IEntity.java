package com.osf.plusalert.public_resources;

public interface IEntity {
    String getEntityName();
}
