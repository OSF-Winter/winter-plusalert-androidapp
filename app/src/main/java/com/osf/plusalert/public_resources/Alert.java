package com.osf.plusalert.public_resources;

import com.osf.plusalert.enumerations.EAlertType;
import com.osf.plusalert.enumerations.EDisasterType;

public class Alert implements IAlert{

    private final Double latitude;
    private final Double longitude;
    private final EAlertType alertType;
    private final String alertName;
    private final String description;
    private final Double radius;
    private final EDisasterType disasterType;

    public Alert(double latitude, double longitude, EAlertType alertType, String alertName,
                 String description, double radius, EDisasterType disasterType){
        this.latitude = latitude;
        this.longitude = longitude;
        this.alertType = alertType;
        this.alertName = alertName;
        this.description = description;
        this.radius = radius;
        this.disasterType = disasterType;
    }

    public Double getLatitude(){
        return latitude;
    }

    public Double getLongitude(){
        return longitude;
    }

    public String getDescription(){
        return description;
    }

    public Double getRadius(){
        return radius;
    }

    public EDisasterType getDisasterType(){
        return disasterType;
    }

    @Override
    public EAlertType getAlertType() {
        return alertType;
    }

    @Override
    public String getEntityName() {
        return alertName;
    }
}
