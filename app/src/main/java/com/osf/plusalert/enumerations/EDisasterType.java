package com.osf.plusalert.enumerations;

public enum EDisasterType {

    FROST("0"),
    HEAT("1"),
    FLOOD("2"),
    AVALANCHE("3"),
    EARTHQUAKE("4"),
    FIRE("5"),
    STORM("6"),
    OTHER("7");


    public static EDisasterType getValue(String alertType){
        switch (alertType){
            case "0":
                return EDisasterType.FROST;

            case "1":
                return EDisasterType.HEAT;

            case "2":
                return EDisasterType.FLOOD;

            case "3":
                return EDisasterType.AVALANCHE;

            case "4":
                return EDisasterType.EARTHQUAKE;

            case "5":
                return EDisasterType.FIRE;

            case "6":
                return EDisasterType.STORM;

            case "7":
                return EDisasterType.OTHER;

            default:
                return null;
        }
    }

    private String alert;

    EDisasterType(String text) {
        this.alert = text;
    }

    public String getAsString() {
        return alert;
    }
}
