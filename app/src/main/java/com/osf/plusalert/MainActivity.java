package com.osf.plusalert;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.osf.plusalert.enumerations.EAlertType;
import com.osf.plusalert.enumerations.EDisasterType;
import com.osf.plusalert.public_resources.Alert;
import com.osf.plusalert.public_resources.CommandCenter;

public class MainActivity extends AppCompatActivity {

    CommandCenter commandCenter = CommandCenter.getInstance();
    private static String imei = null;

    private BroadcastReceiver batteryReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            updateBatteryData(intent);
        }
    };

    private static final int ERROR_DIALOG_REQUEST = 9001;
    private static final int MY_LOCATION_REQUEST_CODE = 1234;
    private boolean wasFireSosSentBefore = false;
    private boolean wasFrostSosSentBefore = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Start on close application service
        startService(new Intent(getBaseContext(), OnClearFromRecentService.class));

        // Start map
        if (isServicesOK()) {
            Intent intent = new Intent(MainActivity.this, MapActivity.class);
            startActivity(intent);
        }

        startReadingBatteryInfo();
        readIMEI();
    }

    private void readIMEI(){
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String[] permissions = {Manifest.permission.READ_PHONE_STATE};

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(MainActivity.this, "IMEI: " + telephonyManager.getDeviceId(), Toast.LENGTH_LONG).show();
            imei = telephonyManager.getDeviceId();
        } else {
            ActivityCompat.requestPermissions(this, permissions, MY_LOCATION_REQUEST_CODE);
        }
    }

    private void startReadingBatteryInfo(){
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_POWER_CONNECTED);
        intentFilter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);

        registerReceiver(batteryReceiver, intentFilter);
    }

    private void updateBatteryData(Intent intent){
        boolean present = intent.getBooleanExtra(BatteryManager.EXTRA_PRESENT, false);
        StringBuilder batteryInfo = new StringBuilder();

        if(present){
            float temperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0) / 10;

            if(temperature > 0 && commandCenter.getActualLongitude() != null && commandCenter.getActualLongitude() != null) {
                batteryInfo.append("Temperature : " + temperature);
//                Toast.makeText(MainActivity.this, String.valueOf(temperature), Toast.LENGTH_LONG).show();

                if(!wasFireSosSentBefore && temperature > 40 && imei != null) {
                    Toast.makeText(MainActivity.this, "You're on fire bro!", Toast.LENGTH_LONG).show();
                    wasFireSosSentBefore = true;

                    Alert alert = new Alert(commandCenter.getActualLatitude(), commandCenter.getActualLongitude(), EAlertType.HIGH,
                            "AUTOMATED ALERT", "I'am on fire!", 10, EDisasterType.FIRE);
                    commandCenter.sendAlert(alert, imei);
                } else if(!wasFrostSosSentBefore && temperature < 27 && imei != null){
                    Toast.makeText(MainActivity.this, "The winter is coming!", Toast.LENGTH_LONG).show();
                    wasFrostSosSentBefore = true;

                    Alert alert = new Alert(commandCenter.getActualLatitude(), commandCenter.getActualLongitude(), EAlertType.HIGH,
                            "AUTOMATED ALERT", "The winter is coming!", 10, EDisasterType.FROST);
                    commandCenter.sendAlert(alert, imei);
                }
            }
        } else {
            Toast.makeText(MainActivity.this, "USB state modified", Toast.LENGTH_LONG).show();
        }
    }


    public boolean isServicesOK() {
        int available = GoogleApiAvailability.getInstance()
                .isGooglePlayServicesAvailable(MainActivity.this);

        if (available == ConnectionResult.SUCCESS) {
            return true;
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(
                    MainActivity.this, available, ERROR_DIALOG_REQUEST);
            dialog.show();
        } else {
            Toast.makeText(this, "You can't make map requests", Toast.LENGTH_SHORT).show();
        }

        return false;
    }
}
