package com.osf.plusalert;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.osf.plusalert.enumerations.EAlertType;
import com.osf.plusalert.enumerations.EDisasterType;
import com.osf.plusalert.public_resources.Alert;
import com.osf.plusalert.public_resources.CommandCenter;
import com.osf.plusalert.public_resources.IAlert;
import com.osf.plusalert.public_resources.IEntity;
import com.osf.plusalert.public_resources.IRefuge;
import com.osf.plusalert.public_resources.Refuge;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private SensorManager sensorManager;
    private Sensor pressureSensor;
    private float[] barometerPressure;
    private boolean isPhoneHumid = true;
    private boolean wasFloodSosSent = false;
    private static String imei = null;
    private Double actualLatitude = null;
    private Double actualLongitude = null;
    private ArrayList<Marker> markerList = new ArrayList<>();

    private void readIMEI() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String[] permissions = {Manifest.permission.READ_PHONE_STATE};

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(MainActivity.this, "IMEI: " + telephonyManager.getDeviceId(), Toast.LENGTH_LONG).show();
            imei = telephonyManager.getDeviceId();
            commandCenter.setImei(imei);
        } else {
            ActivityCompat.requestPermissions(this, permissions, MY_LOCATION_REQUEST_CODE);
        }
    }

    private SensorEventListener sensorEventListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            barometerPressure = sensorEvent.values;
            float pressure = barometerPressure[0];

            if (pressure > 998.58f && isPhoneHumid && !wasFloodSosSent && imei != null
                    && actualLatitude != null && actualLongitude != null) {
                Alert alert = new Alert(actualLatitude, actualLongitude, EAlertType.HIGH,
                        "AUTOMATED ALERT", "", 10, EDisasterType.FLOOD);
                commandCenter.sendAlert(alert, imei);
                wasFloodSosSent = true;
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };

    private static final String TAG = "MapActivity";
    private static final int MY_LOCATION_REQUEST_CODE = 1234;
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Boolean mLocationPermissionsGranted = false;
    private final static float DEFAULT_ZOOM = 15f;
    private List<IAlert> alerts;
    private List<IRefuge> refuges;
    private CommandCenter commandCenter = CommandCenter.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        getLocationPermission();

        readIMEI();

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        pressureSensor = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mLocationPermissionsGranted = false;

        switch (requestCode) {
            case MY_LOCATION_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            mLocationPermissionsGranted = false;
                            return;
                        }
                    }
                    mLocationPermissionsGranted = true;
                    initMap();
                }
            }
        }
    }

    private void moveCamera(LatLng latLng, float zoom, String title) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

        if (!title.equals("My Location")) {
            MarkerOptions options = new MarkerOptions()
                    .position(latLng)
                    .title(title);
            mMap.addMarker(options);
        }

        hideSoftKeyboard();
    }

    private void updateAlerts() {
        try {

            alerts = commandCenter.getAlerts();
            for (IAlert model : alerts) {
                Alert alert = (Alert) model;
                LatLng currentPositionPoint = new LatLng(alert.getLatitude(), alert.getLongitude());

                drawEntity(currentPositionPoint, alert, false);
            }
            commandCenter.getRefuges();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateRefuges() {
        try {
            refuges = commandCenter.getRefuges();
            for (IRefuge model : refuges) {
                Refuge refuge = (Refuge) model;
                LatLng currentPositionPoint = new LatLng(refuge.getLatitude(), refuge.getLongitude());

                drawEntity(currentPositionPoint, refuge, true);
            }
            commandCenter.getRefuges();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void hideSoftKeyboard() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        mapFragment.getMapAsync(MapActivity.this);
    }

    private void getLocationPermission() {
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionsGranted = true;
            initMap();
        } else {
            ActivityCompat.requestPermissions(this, permissions, MY_LOCATION_REQUEST_CODE);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (mLocationPermissionsGranted) {
            moveCursorToCurrentLocation();

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);

            updateAlerts();
            updateRefuges();
        }

        //Start update thread
        new Timer().scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                commandCenter.updateClientLocation();
            }
        }, 10000, 5000);

        boolean userExists = commandCenter.checkIfUserExists(imei);
        if (!userExists) {
            Intent intent = new Intent(MapActivity.this, ActivityLogin.class);
            startActivity(intent);
        }
    }

    public void clearMarkers() {
        mMap.clear();
    }

    public void updateLocation() {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        try {
            if (mLocationPermissionsGranted) {
                final Task location = mFusedLocationProviderClient.getLastLocation();

                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            Location currentLocation = (Location) task.getResult();

                            actualLatitude = currentLocation.getLatitude();
                            actualLongitude = currentLocation.getLongitude();

                            commandCenter.setActualLongitude(actualLongitude);
                            commandCenter.setActualLatitude(actualLatitude);
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e(TAG, "moveCursorToCurrentLocation: SecurityException: " + e.getMessage());
        }
    }

    private void moveCursorToCurrentLocation() {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        try {
            if (mLocationPermissionsGranted) {

                final Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            Location currentLocation = (Location) task.getResult();
                            LatLng currentPositionPoint = new LatLng(currentLocation
                                    .getLatitude(), currentLocation.getLongitude());

                            actualLatitude = currentLocation.getLatitude();
                            actualLongitude = currentLocation.getLongitude();

                            commandCenter.setActualLongitude(actualLongitude);
                            commandCenter.setActualLatitude(actualLatitude);

                            moveCamera(currentPositionPoint, DEFAULT_ZOOM, "My Location");
                        } else {
                            Toast.makeText(MapActivity.this,
                                    "unable to get current location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e(TAG, "moveCursorToCurrentLocation: SecurityException: " + e.getMessage());
        }
    }

    public void drawEntity(LatLng currentPositionPoint, IEntity entity, boolean isRefuge) {
        Marker marker;

        if (!isRefuge) {
            Alert alert = (Alert) entity;

            mMap.addCircle(new CircleOptions()
                    .center(currentPositionPoint)
                    .radius(alert.getRadius())
                    .strokeColor(Color.BLACK)
                    .strokeWidth(1f)
                    .fillColor(EAlertType.getColor(alert.getAlertType().getAsString())));

            marker = mMap.addMarker(new MarkerOptions().position(currentPositionPoint)
                    .title(alert.getEntityName()));
            marker.showInfoWindow();
        } else {
            Refuge refuge = (Refuge) entity;

            mMap.addCircle(new CircleOptions()
                    .center(currentPositionPoint)
                    .radius(30)
                    .strokeColor(Color.BLACK)
                    .strokeWidth(0f)
                    .fillColor(0x44007000));
            marker = mMap.addMarker(new MarkerOptions().position(currentPositionPoint)
                    .title(refuge.getEntityName()));
            marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.refuge_marker2));
        }

        markerList.add(marker);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        sensorManager.registerListener(sensorEventListener, pressureSensor, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(sensorEventListener);
    }

}
